package com.devcamp.s50.jbr3_90;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/tinhdientich")
    public double tinhdientich(){
        Rectangle HinhChuNhat1 = new Rectangle(24 , 12);
        HinhChuNhat1.getDienTich();
        
        return HinhChuNhat1.getDienTich() ;
    }

    @CrossOrigin
    @GetMapping("/tinhchuvi")
    public double tinhchuvi(){
        Rectangle HinhChuNhat2 = new Rectangle(15 , 12);
        HinhChuNhat2.getDienTich();

        return HinhChuNhat2.getChuVi() ;
    }
}
