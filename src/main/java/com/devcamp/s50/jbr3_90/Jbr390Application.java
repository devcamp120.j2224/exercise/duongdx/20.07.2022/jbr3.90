package com.devcamp.s50.jbr3_90;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr390Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr390Application.class, args);
	}

}
